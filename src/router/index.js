import Vue from 'vue'
import Router from 'vue-router'
import Signature from '@/components/Signature'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Signature',
      component: Signature
    }
  ]
})
